##Starting
**Requirements**

* **Setup**: https://github.com/libgdx/libgdx/wiki/Setting-up-your-Development-Environment-(Eclipse,-Intellij-IDEA,-NetBeans)#setting-up-intellij-idea
    * Java8 SDK (minimum 7) http://www.oracle.com/technetwork/java/javase/downloads/index.html
    * IntelliJ (community edition should suffice) (android studio, netbeans and eclipse should work too but I used IntelliJ 2017.1)
    * Android SDK 
        * => Comes with android studio => Use SDK manager to install latest stable platform the steps are:
            * Download android studio and run it,
            * Opening screen will give a list of actions but at the bottom right there should be *Configue*
            * Click *Configure* -> *SDK Manager*
            * There select *Android 7.1.1* and *Android 8.1* (optionally choose older versions in order to give more support)
            * Under SDK Tools select *Android SDK Build-Tools 28-rc1*, *Android Emulator*, *Android SDK Platform-Tools 27.0.1*, *Android SDK Tools*, *Documentation for Android SDK*, *Google USB Driver, rev 11* and *Intel x86 Emulator Accelerator (HAXM installer)*
            * Press *apply*, then 'read' and accept terms and conditions for every one of them
        * => You also have to create an environment variable called ANDROID_HOME, which points at your Android SDK installation directory (It says so in the libGDX wiki but I didn't need it)
    * Extra ifo under the github link
* **Creating project**: https://github.com/libgdx/libgdx/wiki/Project-Setup-Gradle
    * Download: https://libgdx.badlogicgames.com/nightlies/dist/gdx-setup.jar
    * Open up and fill in to your desire
        * SDK location is something like C:\Users\username\AppData\Local\Android\sdk
        * If it gives errors about android(sdk) it is possible you need to download something extra or updata via the android sdk manager
        * Google the error you get...
    * Open up IntelliJ and import the folder where your new project was created. Alternative is to use gradle by selecting the *build.gradle* file
        * Using gradle:  https://github.com/libgdx/libgdx/wiki/Gradle-and-Intellij-IDEA
* **First time running desktop launcher**
    * If no configuration for the DesktopLauncher is set, run the DesktopLauncher main method
    * Edit configuration for desktoplauncher => change working directory to the android/assets folder, eg: C:\Users\username\Downloads\TestGame\android\assets
    * Run it, it should open a screen with the badlogic logo which can be found inside the android/assets folder
    * From here on you can start developing

##Creating apk for android

Execute the following to create an unsigned apk. Execute the command in the directory from your project using git bash or just command line.
The directory from your project contains the folders android, core, desktop,...

*./gradlew android:assembleRelease*

This creates an unsigned apk which you can transfer to your android phone. In order to install and run it you will need to have developer options on with debugging settings on.
 How to to this is dependant on your phone and android version.

##Signing
In order to get your application / apk on the android market (aka google play store) you will need to sign it.
Used links for instructions and trouble shooting:
* http://www.instructables.com/id/How-to-Make-an-Android-Game-Snake/
* https://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10
* https://stackoverflow.com/questions/38714651/android-studio-dexindexoverflowexception-method-id-not-in
* https://stackoverflow.com/questions/47291056/could-not-find-tools-jar-please-check-that-c-program-files-java-jre1-8-0-151-c?rq=1
* https://stackoverflow.com/questions/11345193/gradle-does-not-find-tools-jar
* https://stackoverflow.com/questions/4830253/where-is-the-keytool-application
* https://stackoverflow.com/questions/6211919/errorkeytool-is-not-recognized-as-an-internal-or-external-command-operable-p
* https://stackoverflow.com/questions/40004884/cant-find-apksigner-executable-to-manually-sign-apk

Based on instructions from http://www.instructables.com/id/How-to-Make-an-Android-Game-Snake/

Execute the following to create an unsigned apk. Execute the command in the directory from your project. The directory containing android, core, desktop,...

*./gradlew android:assembleRelease*

If Gradle can't find tools.jar => set JAVA_HOME as a system property pointing to your SDK directory.

In order to sign the apk you need to 'zipalign' it, it is used to optimize the apk. Go to the directory *build-tools/xx.x.x* from your android directory. For me it was *C:\Users\Username\AppData\Local\Android\sdk\build-tools\25.0.3* 
There you can find *apksigner.bat* and *zipalign.exe* 
Copy these to your *android.build/outputs/apk* folder. There should be android-unsigned-aligned.apk file from executing the gradle command.
Now execute the following to 'zipalign'.

*./zipalign -v -p 4 android-release-unsigned.apk android-unsigned-aligned.apk*

TODO: further fill in instructions

Generating keystore: 

*keytool -genkey -v -keystore chooseName.jks -keyalg RSA -keysize 2048 -validity 10000 -alias chooseName2*

Sign apk with apksigner. Run apksigner with this or copy your .apk file to the build-tools/xx.x.x folder
Run inside folder *C:\Users\username\Downloads\TestGame\android\build\outputs\apk*
Instead of ./apksigner use *C:\Users\username\AppData\Local\Android\sdk\build-tools\25.0.3\apksigner*

The command to sign:

*./apksigner sign --ks yourName.jks --out release.apk android-unsigned-aligned.apk*

The command to verify if the apk is signed properly:

*./apksigner verify release.apk*

It is successful if no output is given after you press enter.


##-----------------------------------------------------

*Standard stuff from bitbucket*

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).