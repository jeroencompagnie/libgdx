package com.mygdx.game;

import com.badlogic.gdx.Game;

public class MyGame extends Game {

    public static final int WIDTH = 800, HEIGHT = 480; // used later to set window size

    public MyGame() {

    }

    @Override
    public void create() {
        this.setScreen(new SplashScreen(this));
    }

    //New screens can be added with classes.
    //Implement screen and then use this class to set the class as current screen.
    public void loadMainMenu() {
        this.setScreen(new MainMenu(this));
    }

    public void play() {
        this.setScreen(new GameMode(this));
    }
}
