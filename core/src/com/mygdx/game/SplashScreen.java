package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class SplashScreen implements Screen {
    private Texture splashScreen = new Texture(Gdx.files.internal("slack-imgs.jpg"));
    private Image splashImage = new Image(splashScreen);
    private Stage stage;
    private MyGame myGame;
    private StretchViewport viewport;

    public SplashScreen(MyGame myGame) {
        this.myGame = myGame;
        viewport = new StretchViewport(this.myGame.WIDTH, this.myGame.HEIGHT);
        stage = new Stage(viewport);
    }

    // Constructor of the stage
    @Override
    public void show() {
        stage.addActor(splashImage);
        splashImage.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(0.5f), Actions.delay(1.2f), Actions.fadeOut(0.5f), Actions.run(new Runnable() {
            @Override
            public void run() {
                myGame.loadMainMenu();
            }
        })));
    }

    @Override
    public void render(float delta) {
        // Clear actors from previous screens
        Gdx.gl.glClearColor(0, 0, 0, 1); //sets clear color to black
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    // clean up after closing of the screen
    @Override
    public void dispose() {
        splashScreen.dispose();
        stage.dispose();
    }
}
