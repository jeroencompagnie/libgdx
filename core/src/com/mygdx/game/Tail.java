package com.mygdx.game;


import com.badlogic.gdx.math.Rectangle;

public class Tail extends Rectangle implements Snek {
    private float distanceBetween = 30;
    private double previousDirection;
    private float PLAYERMOVEMENTSPEED = 100;

    public Tail(Snek player) {
        previousDirection = player.getDirection();
//        currentDirection += 2 * Math.PI;
        this.x = player.getX() - player.getHeight() * (float) Math.cos(previousDirection + 2 * Math.PI);
        this.y = player.getY() - player.getHeight() * (float) Math.sin(previousDirection + 2 * Math.PI);
        this.width = player.getWidth();
        this.height = player.getHeight();
//        System.err.println("break");
    }

    public void moveTail(Snek previousTail, float delta) {
        this.x += PLAYERMOVEMENTSPEED * delta * (float) Math.cos(previousDirection);
        this.y += PLAYERMOVEMENTSPEED * delta * (float) Math.sin(previousDirection);
        if (this.x <0) {
            x = MyGame.WIDTH;
        } else if(this.x > MyGame.WIDTH) {
            x  = 0;
        } else if(this.y < 0) {
            y = MyGame.HEIGHT;
        } else if(this.y > MyGame.HEIGHT) {
            y = 0;
        }

        previousDirection = previousTail.getDirection();
    }

    @Override
    public double getDirection() {
        return previousDirection;
    }
}
