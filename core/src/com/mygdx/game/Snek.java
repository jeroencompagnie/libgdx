package com.mygdx.game;

public interface Snek {
    public double getDirection();
    public float getX();
    public float getY();
    public float getWidth();
    public float getHeight();
}
