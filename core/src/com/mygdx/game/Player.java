package com.mygdx.game;

import com.badlogic.gdx.math.Rectangle;

public class Player extends Rectangle {

    public Player(float x, float y) {
        super(x, y, 10, 10);
    }
}
