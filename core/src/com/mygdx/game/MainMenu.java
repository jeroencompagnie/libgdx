package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

public class MainMenu implements Screen {

    private final MyGame myGame;
    private Texture playTexture = new Texture(Gdx.files.internal("play.png"));
    private Image playImage = new Image(playTexture);
    private ScalingViewport m_viewport = new ScalingViewport(Scaling.stretch, MyGame.WIDTH, MyGame.HEIGHT);
    private Stage stage = new Stage(m_viewport);

    public MainMenu(MyGame myGame) {
        this.myGame = myGame;
    }

    @Override
    public void show() {
        playImage.setX(MyGame.WIDTH / 2 - playImage.getWidth() / 2);
        playImage.setY(MyGame.HEIGHT / 2 - playImage.getHeight() / 2);

        playImage.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                myGame.play();
            }
        });

        stage.addActor(playImage);

        Gdx.input.setInputProcessor(stage); // In order to catch touch events
    }

    @Override
    public void render(float delta) {
        // Clear actors from previous screens
        Gdx.gl.glClearColor(0, 0, 0, 1); //sets clear color to black
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(); //update all actors
        stage.draw(); //draw all actors on the Stage.getBatch()
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        playTexture.dispose();

        stage.dispose();
    }
}
