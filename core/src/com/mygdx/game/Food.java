package com.mygdx.game;

import com.badlogic.gdx.math.Rectangle;

public class Food extends Rectangle {
    public Food() {
        super(0,0, 15, 15);
        spawnFood();

    }

    private void spawnFood() {
        x = (float) Math.random() * MyGame.WIDTH-15;
        y = (float) Math.random() * MyGame.HEIGHT-15;
    }

    private boolean isEaten(Rectangle player) {
        return this.overlaps(player);
    }

    public boolean handelFoodLogic(Player player) {
        if(isEaten(player)) {
            spawnFood();
            return true;
        }
        return false;
    }
}
