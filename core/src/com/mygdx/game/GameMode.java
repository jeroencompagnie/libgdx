package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.LinkedList;

import static com.mygdx.game.MyGame.HEIGHT;
import static com.mygdx.game.MyGame.WIDTH;

public class GameMode implements Screen {
    private final MyGame myGame;
    private final ShapeRenderer shapeRenderer;
    private final StretchViewport viewport;
    private final Stage stage;
    private final OrthographicCamera camera;
    private float r = 10;
    private float x = WIDTH/2-r;
    private float y = HEIGHT/2-r;
    private Food food;
    private LinkedList<Player> snake;
    private double currentDirection = Math.PI;
    private float timer = 0;
    private int snakeLength;


    public GameMode(MyGame myGame) {
        this.myGame = myGame;
        this.shapeRenderer = new ShapeRenderer();
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, WIDTH, HEIGHT);
        this.viewport = new StretchViewport(WIDTH, HEIGHT, camera);
        this.stage = new Stage(viewport);
        snake = new LinkedList<Player>();
        snake.add(new Player(300, 300));
        snake.add(new Player(310, 300));
        snake.add(new Player(320, 300));
        snakeLength = snake.size();
        this.food = new Food();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1); //sets clear color to black
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //clear the batch
        handleGameLogic(delta);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        for(Player player: snake) {
            shapeRenderer.rect(player.x, player.y, player.width, player.height);
        }
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(food.x, food.y, food.width, food.height);
//        renderTail();
        shapeRenderer.end();

        stage.act();
        stage.draw();
    }

//    private void renderTail() {
//        shapeRenderer.setColor(Color.WHITE);
//        for (Tail tailRect : player.getTail()) {
//            shapeRenderer.rect(tailRect.x, tailRect.y, tailRect.width, tailRect.height);
//        }
//    }

    private void handleGameLogic(float delta) {
        timer += delta;
        if(timer > 0.08f) {
            timer = 0;
            advance();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            currentDirection += Math.PI / 2;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            currentDirection -= Math.PI / 2;
        }
    }

    private void advance() {
        float headX = snake.get(0).getX();
        float headY = snake.get(0).getY();
        float addX = (float) (10 * Math.cos(currentDirection));
        float addY = (float) (10 * Math.sin(currentDirection));
        float newX = headX + addX;
        float newY = headY + addY;
        if(newX < 0) {
            newX = WIDTH;
        } else if(newX > WIDTH) {
            newX = 0;
        }
        if(newY < 0) {
            newY = HEIGHT;
        } else if(newY > WIDTH) {
            newY = 0;
        }
        snake.add(0, new Player(newX, newY));
//        switch(currentDirection) {
//            case 0: //up
//                snake.addFirst(new Player(headX, headY+10));
//                break;
//            case 1: //right
//                snake.addFirst(new Player(headX+10, headY));
//                break;
//            case 2: //down
//                snake.addFirst(new Player(headX, headY-10));
//                break;
//            case 3: //left
//                snake.addFirst(new Player(headX-10, headY));
//                break;
//            default://should never happen
//                snake.addFirst(new Player(headX, headY+10));
//                break;
//        }
        if(food.handelFoodLogic(snake.get(0)))  {
            snakeLength++;
        }
        if (snake.size() > snakeLength) {
            snake.removeLast();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
